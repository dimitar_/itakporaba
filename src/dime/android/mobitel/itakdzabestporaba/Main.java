package dime.android.mobitel.itakdzabestporaba;

import dime.android.mobitel.itakdzabestporaba.R;
import dime.android.mobitel.itakdzabestporaba.widget.ItakPorabaWidgetService;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Main extends Activity
{
	private static final String FIRST_RUN_SETTINGS_KEY = "firstRun";
	
	TextView poraba;
	TextView porabaMoneta;
	TextView skupaj;

	TextView mobitel;
	TextView sms;
	TextView internet;
	TextView ostala;

	TextView refreshDate;

	ProgressBar mobitelBar;
	ProgressBar smsBar;
	ProgressBar internetBar;
	ProgressBar ostalaBar;

	private void checkFirstRun ()
	{
		/*
		 * IF this is the first time the application is started -- fetch the data
		 */
		SharedPreferences settings = getSharedPreferences(LocalCache.PREFS_NAME, Context.MODE_PRIVATE);
		boolean firstRun = settings.getBoolean(FIRST_RUN_SETTINGS_KEY, true);
		
		if (firstRun)
		{
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean(FIRST_RUN_SETTINGS_KEY, false);
			editor.commit();
			
			refreshData(true);
		}
	}
	
	private void refreshData(boolean displayError)
	{
		new DataFetcher(this, displayError).execute();
	}
	
	private void openPreferencesActivity()
	{
		Intent intent = new Intent(this, Preferences.class);
		startActivityForResult(intent, 0);
	}

	public void updateUI(Podatki podatki)
	{
		this.poraba.setText(podatki.getPoraba() + " €");
		this.porabaMoneta.setText(podatki.getPorabaMoneta() + " €");
		this.skupaj.setText(Podatki.TWO_DECIMALS_FORMAT.format(podatki.getPoraba() + podatki.getPorabaMoneta()) + " €");

		this.mobitelBar.setMax(podatki.getMobitelAll());
		this.mobitelBar.setProgress(podatki.getMobitelUsed());
		this.mobitel.setText(podatki.getMobitelUsed() + " / " + podatki.getMobitelAll() + " min");

		this.smsBar.setMax(podatki.getSmsAll());
		this.smsBar.setProgress(podatki.getSmsUsed());
		this.sms.setText(podatki.getSmsUsed() + " / " + podatki.getSmsAll());

		this.internetBar.setMax(podatki.getInternetAll());
		this.internetBar.setProgress(podatki.getInternetUsed());
		this.internet.setText(podatki.getInternetUsed() + " / " + podatki.getInternetAll() + " MB");

		this.ostalaBar.setMax(podatki.getOstalaOmrAll());
		this.ostalaBar.setProgress(podatki.getOstalaOmrUsed());
		this.ostala.setText(podatki.getOstalaOmrUsed() + " / " + podatki.getOstalaOmrAll() + " min");
		
		this.refreshDate.setText(podatki.getRefreshDate());

		/*
		 * Update the widget.
		 */
		this.getApplicationContext().startService(new Intent(this.getApplicationContext(), ItakPorabaWidgetService.class));
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		this.poraba = (TextView) this.findViewById(R.id.poraba_value);
		this.porabaMoneta = (TextView) this.findViewById(R.id.poraba_moneta_value);
		this.skupaj = (TextView) this.findViewById(R.id.skupaj_value);

		this.mobitel = (TextView) this.findViewById(R.id.mobitel_value);
		this.sms = (TextView) this.findViewById(R.id.sms_value);
		this.internet = (TextView) this.findViewById(R.id.internet_value);
		this.ostala = (TextView) this.findViewById(R.id.ostala_value);
		
		this.refreshDate = (TextView) this.findViewById(R.id.refresh_date);

		this.mobitelBar = (ProgressBar) this.findViewById(R.id.mobitel_progress);
		this.smsBar = (ProgressBar) this.findViewById(R.id.sms_progress);
		this.internetBar = (ProgressBar) this.findViewById(R.id.internet_progress);
		this.ostalaBar = (ProgressBar) this.findViewById(R.id.ostala_progress);

		checkFirstRun();
		
		/*
		 * Retrieve from the local cache
		 */
		this.updateUI(LocalCache.getLocalData(this));

		/*
		 * Refresh from planet.mobitel.si
		 */
		// XXX this.refreshData ( );
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case R.id.refresh:
			this.refreshData(true);
			return true;
		case R.id.settings:
			this.openPreferencesActivity();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}