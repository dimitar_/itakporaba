/**
 * @author dime
 * 
 *         Dec 13, 2010 -- 10:29:50 PM
 */
package dime.android.mobitel.itakdzabestporaba;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;

public class PlanetParser
{

	private static final String	USER_AGENT					= "Mozilla/5.0 (Linux; U; Android 0.5; en-US) AppleWebKit/522+ (KHTML, like Gecko) Safari/419.3";

	// The first URL for logging
	private static final String	MAIN_URL						= "http://planet.mobitel.si";
	private static final String	MENU_URL						= "/Mp.aspx?ni=1482";

	private static final String	A_HREF_PATTERN_START		= ".*<a href=\"(.*)\">";
	private static final String	A_HREF_PATTERN_END		= "</a>.*";

	private static final String	ZAKUPLJENE_KOLICINE		= "Zakupljene količine";
	private static final String	INFORMATIVNA_PORABA		= "Informativna mesečna poraba";

	private static final String	SMS_PORABA					= ".*Informativno stanje neporabljene zakupljene količine za SMS in MMS ([0-9]*?) za (.*?) je ([0-9]*\\.?[0-9]*?) sporočil.*";
	private static final String	MOBITEL_PORABA				= ".*Informativno stanje neporabljene zakupljene količine za Klici v omr\\. Mobitel ([0-9]*?) min za (.*?) je ([0-9]*\\.?[0-9]*?) min.*";
	private static final String	OSTALA_OMREŽJA_PORABA	= ".*Informativno stanje neporabljene zakupljene količine za Klici v druga mob\\., stac\\. ([0-9]*?) min za (.*?) je ([0-9]*?) min.*";
	private static final String	INTERNET_PORABA			= ".*Informativno stanje neporabljene zakupljene količine za Itak Džabest ([0-9]*?) MB za (.*?) je ([0-9]*?) MB.*";

	private static final String	MESECNA_PORABA				= ".*Informativna poraba tekočega meseca za opravljene storitve GSM/UMTS za številko (.*) je ([0-9]*,[0-9]*) EUR, za opravljene nakupe prek Monete pa ([0-9]*,[0-9]*) EUR.*";


	private static Map<String, String> parseLinksFromMenu (String html)
	{
		Map<String, String> map = new HashMap<String, String> ( );

		Pattern pattern;
		Matcher matcher;
		html = html.replaceAll ("\n", " ");

		/*
		 * Search for ZAKUPLJENE_KOLICINE link
		 */
		pattern = Pattern.compile (PlanetParser.A_HREF_PATTERN_START + PlanetParser.ZAKUPLJENE_KOLICINE
				+ PlanetParser.A_HREF_PATTERN_END, Pattern.MULTILINE);
		matcher = pattern.matcher (html);

		if (matcher.matches ( ))
		{
			map.put (PlanetParser.ZAKUPLJENE_KOLICINE, matcher.group (1));
		}

		/*
		 * Search for INFORMATIVNA_PORABA link
		 */
		pattern = Pattern.compile (PlanetParser.A_HREF_PATTERN_START + PlanetParser.INFORMATIVNA_PORABA
				+ PlanetParser.A_HREF_PATTERN_END, Pattern.MULTILINE);
		matcher = pattern.matcher (html);

		if (matcher.matches ( ))
		{
			map.put (PlanetParser.INFORMATIVNA_PORABA, matcher.group (1));
		}

		return map;
	}


	private static String streamToString (InputStream stream) throws Exception
	{
		InputStreamReader reader = new InputStreamReader (stream, "UTF8");
		BufferedReader buffer = new BufferedReader (reader);
		StringBuilder sb = new StringBuilder ( );

		try
		{
			String cur;
			while ((cur = buffer.readLine ( )) != null)
			{
				sb.append (cur + "\n");
			}
		}
		catch (IOException e)
		{
			e.printStackTrace ( );
		}

		try
		{
			stream.close ( );
		}
		catch (IOException e)
		{
			e.printStackTrace ( );
		}

		return sb.toString ( ).replaceAll ("&amp;", "&");
	}


	private static void addReferer (HttpGet get, String referer)
	{
		if (referer != null)
		{
			get.addHeader ("Referer", referer);
		}
	}


	private static void parseZakupljeneKolicine (Podatki podatki, String html)
	{
		Pattern pattern;
		Matcher matcher;
		html = html.replaceAll ("\n", " ");

		/*
		 * Search for INTERNET
		 */
		pattern = Pattern.compile (PlanetParser.INTERNET_PORABA);
		matcher = pattern.matcher (html);

		if (matcher.matches ( ))
		{
			String skupajStr = matcher.group (1);
			String neporabljeneStr = matcher.group (3);

			int skupaj = Integer.parseInt (skupajStr);
			int neporabljeno = Integer.parseInt (neporabljeneStr);

			podatki.setInternetAll (skupaj);
			podatki.setInternetUsed (skupaj - neporabljeno);
		}

		/*
		 * Search for MOBITEL
		 */
		pattern = Pattern.compile (PlanetParser.MOBITEL_PORABA);
		matcher = pattern.matcher (html);

		if (matcher.matches ( ))
		{
			String skupajStr = matcher.group (1);
			String neporabljeneStr = matcher.group (3);
			
			neporabljeneStr = neporabljeneStr.replaceAll ("\\.", "");

			int skupaj = Integer.parseInt (skupajStr);
			int neporabljeno = Integer.parseInt (neporabljeneStr);

			podatki.setMobitelAll (skupaj);
			podatki.setMobitelUsed (skupaj - neporabljeno);
		}

		/*
		 * Search for SMS
		 */
		pattern = Pattern.compile (PlanetParser.SMS_PORABA);
		matcher = pattern.matcher (html);

		if (matcher.matches ( ))
		{
			String skupajStr = matcher.group (1);
			String neporabljeneStr = matcher.group (3);
			
			neporabljeneStr = neporabljeneStr.replaceAll ("\\.", "");

			int skupaj = Integer.parseInt (skupajStr);
			int neporabljeno = Integer.parseInt (neporabljeneStr);

			podatki.setSmsAll (skupaj);
			podatki.setSmsUsed (skupaj - neporabljeno);
		}

		/*
		 * Search for OSTALA OMR
		 */
		pattern = Pattern.compile (PlanetParser.OSTALA_OMREŽJA_PORABA);
		matcher = pattern.matcher (html);

		if (matcher.matches ( ))
		{
			String skupajStr = matcher.group (1);
			String neporabljeneStr = matcher.group (3);

			int skupaj = Integer.parseInt (skupajStr);
			int neporabljeno = Integer.parseInt (neporabljeneStr);

			podatki.setOstalaOmrAll (skupaj);
			podatki.setOstalaOmrUsed (skupaj - neporabljeno);
		}
	}


	private static void parseInformativnaPoraba (Podatki podatki, String html)
	{
		Pattern pattern;
		Matcher matcher;
		html = html.replaceAll ("\n", " ");

		/*
		 * Search for the pattern
		 */
		pattern = Pattern.compile (PlanetParser.MESECNA_PORABA);
		matcher = pattern.matcher (html);

		if (matcher.matches ( ))
		{
			String moneta = matcher.group (3);
			String drugo = matcher.group (2);
			moneta = moneta.replaceAll (",", ".");
			drugo = drugo.replaceAll (",", ".");

			float monetaF = Float.parseFloat (moneta);
			float drugoF = Float.parseFloat (drugo);

			podatki.setPoraba (drugoF);
			podatki.setPorabaMoneta (monetaF);
		}
	}


	private static void chechStatusCode (HttpResponse response) throws Exception
	{
		int statusCode = response.getStatusLine ( ).getStatusCode ( );

		if (statusCode != 200)
		{
			throw new Exception ("Connection error");
		}
	}


	public static Podatki getData ( ) throws Exception
	{
		URI uri;
		HttpGet getRequest;
		HttpResponse res;
		HttpEntity entity;
		InputStream data;
		String html;

		Podatki podatki = new Podatki ( );

		DefaultHttpClient client = new DefaultHttpClient ( );
		client.getParams ( ).setParameter (ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);
		client.getParams ( ).setParameter ("http.useragent", PlanetParser.USER_AGENT);

		/*
		 * Make a request for the first MENU page
		 */
		uri = new URI (PlanetParser.MAIN_URL + PlanetParser.MENU_URL);
		getRequest = new HttpGet (uri);
		res = client.execute (getRequest);
		PlanetParser.chechStatusCode (res);
		entity = res.getEntity ( );
		data = entity.getContent ( );
		html = PlanetParser.streamToString (data);
		entity.consumeContent ( );

		/*
		 * Get the links to the desired pages
		 */
		Map<String, String> links = PlanetParser.parseLinksFromMenu (html);

		/*
		 * Fetch the page ZAKUPLJENE_KOLICINE
		 */
		uri = new URI (PlanetParser.MAIN_URL + links.get (PlanetParser.ZAKUPLJENE_KOLICINE));
		getRequest = new HttpGet (uri);
		PlanetParser.addReferer (getRequest, PlanetParser.MAIN_URL + PlanetParser.MENU_URL);
		res = client.execute (getRequest);
		PlanetParser.chechStatusCode (res);
		entity = res.getEntity ( );
		data = entity.getContent ( );
		html = PlanetParser.streamToString (data);
		entity.consumeContent ( );

		// Parse ZAKUPLJENE_KOLICINE
		PlanetParser.parseZakupljeneKolicine (podatki, html);

		/*
		 * Fetch the page INFORMATIVNA_PORABA
		 */
		uri = new URI (PlanetParser.MAIN_URL + links.get (PlanetParser.INFORMATIVNA_PORABA));
		getRequest = new HttpGet (uri);
		PlanetParser.addReferer (getRequest, PlanetParser.MAIN_URL + PlanetParser.MENU_URL);
		res = client.execute (getRequest);
		PlanetParser.chechStatusCode (res);
		entity = res.getEntity ( );
		data = entity.getContent ( );
		html = PlanetParser.streamToString (data);
		entity.consumeContent ( );

		// Parse INFORMATIVNA_PORABA
		PlanetParser.parseInformativnaPoraba (podatki, html);
		
		// Add the refresh date
		podatki.setRefreshDate(Podatki.DATE_FORMATTER.format(new Date()));

		return podatki;
	}

}
