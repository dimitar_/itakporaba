package dime.android.mobitel.itakdzabestporaba;

import java.util.Calendar;

import dime.android.mobitel.itakdzabestporaba.widget.ItakPorabaWidgetService;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AutoUpdater
{
	public static final String ACTIVE_UPDATE_INTERVAL_KEY = "refreshInterval";
	public static final String UPDATE_DATA_INTENT_EXTRA_KEY = "updateData";
	
	public static void setNewAutoRefreshInterval(Context context, int selectedInterval)
	{
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		
		// Cancel the pending intent
		Intent newIntent = new Intent(context, ItakPorabaWidgetService.class);
		newIntent.putExtra(UPDATE_DATA_INTENT_EXTRA_KEY, true);
		PendingIntent pendingIntent = PendingIntent.getService(context, 0, newIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		
		if (selectedInterval > 0)
		{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, selectedInterval);
			
			alarmManager.setRepeating(AlarmManager.RTC, cal.getTimeInMillis(), selectedInterval * 60 * 1000, pendingIntent);
			Log.i("ItakDzabestPorabaAutoRefresh", "Registered repeating alarm with interval " + selectedInterval + " and first start @ " + cal.getTime().toString());
		}
		else
		{
			alarmManager.cancel(pendingIntent);
		}
	}
}
