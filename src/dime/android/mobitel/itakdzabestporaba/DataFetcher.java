/**
 * @author dime
 * 
 *         Dec 15, 2010 -- 11:30:03 PM
 */
package dime.android.mobitel.itakdzabestporaba;

import android.os.AsyncTask;
import android.widget.Toast;

public class DataFetcher extends AsyncTask<Void, Void, Podatki>
{

	private boolean error;
	private boolean displayError;

	private Main main;

	public DataFetcher(Main main, boolean displayError)
	{
		this.main = main;
		this.displayError = displayError;
	}

	@Override
	protected void onPreExecute()
	{
		this.main.setTitle(this.main.getString(R.string.app_name) + " - " + this.main.getString(R.string.loading));
	}

	@Override
	protected Podatki doInBackground(Void... arg0)
	{
		Podatki podatki = null;
		this.error = false;

		try
		{
			podatki = PlanetParser.getData();
			LocalCache.storeLocalData(this.main, podatki);
		}
		catch (Exception e)
		{
			this.error = true;
		}

		return podatki;
	}

	@Override
	protected void onPostExecute(Podatki podatki)
	{
		if (!this.error)
			this.main.updateUI(podatki);
		else
			if (displayError)
				Toast.makeText(this.main, this.main.getString(R.string.connection_error), Toast.LENGTH_LONG).show();

		this.main.setTitle(this.main.getString(R.string.app_name));
	}

}
