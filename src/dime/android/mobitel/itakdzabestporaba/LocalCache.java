/**
 * @author dime
 * 
 *         Dec 15, 2010 -- 11:26:46 PM
 */
package dime.android.mobitel.itakdzabestporaba;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalCache
{

	public static final String PREFS_NAME = "ItakDzabestPorabaSettings";

	private static final String REFRESH_DATE = "refreshDate";

	private static final String PORABA = "poraba";
	private static final String MONETA = "moneta";

	private static final String MOBITEL_ALL = "mobitelAll";
	private static final String MOBITEL_USED = "mobitelUsed";

	private static final String SMS_ALL = "smsAll";
	private static final String SMS_USED = "smsUsed";

	private static final String INTERNET_ALL = "internetAll";
	private static final String INTERNET_USED = "internetUsed";

	private static final String OSTALA_ALL = "ostalaAll";
	private static final String OSTALA_USED = "ostalaUsed";

	public static Podatki getLocalData(Context context)
	{
		// Restore preferences
		SharedPreferences settings = context.getSharedPreferences(LocalCache.PREFS_NAME, Context.MODE_PRIVATE);

		Podatki podatki = new Podatki();

		podatki.setPoraba(settings.getFloat(LocalCache.PORABA, 0.0f));
		podatki.setPorabaMoneta(settings.getFloat(LocalCache.MONETA, 0.0f));

		podatki.setMobitelAll(settings.getInt(LocalCache.MOBITEL_ALL, 0));
		podatki.setMobitelUsed(settings.getInt(LocalCache.MOBITEL_USED, 0));

		podatki.setSmsAll(settings.getInt(LocalCache.SMS_ALL, 0));
		podatki.setSmsUsed(settings.getInt(LocalCache.SMS_USED, 0));

		podatki.setInternetAll(settings.getInt(LocalCache.INTERNET_ALL, 0));
		podatki.setInternetUsed(settings.getInt(LocalCache.INTERNET_USED, 0));

		podatki.setOstalaOmrAll(settings.getInt(LocalCache.OSTALA_ALL, 0));
		podatki.setOstalaOmrUsed(settings.getInt(LocalCache.OSTALA_USED, 0));
		
		podatki.setRefreshDate(settings.getString(LocalCache.REFRESH_DATE, "01.01.1990 00:00:00"));

		return podatki;
	}

	public static void storeLocalData(Context context, Podatki podatki)
	{
		SharedPreferences settings = context.getSharedPreferences(LocalCache.PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();

		editor.putFloat(LocalCache.PORABA, podatki.getPoraba());
		editor.putFloat(LocalCache.MONETA, podatki.getPorabaMoneta());

		editor.putInt(LocalCache.MOBITEL_ALL, podatki.getMobitelAll());
		editor.putInt(LocalCache.MOBITEL_USED, podatki.getMobitelUsed());

		editor.putInt(LocalCache.SMS_ALL, podatki.getSmsAll());
		editor.putInt(LocalCache.SMS_USED, podatki.getSmsUsed());

		editor.putInt(LocalCache.INTERNET_ALL, podatki.getInternetAll());
		editor.putInt(LocalCache.INTERNET_USED, podatki.getInternetUsed());

		editor.putInt(LocalCache.OSTALA_ALL, podatki.getOstalaOmrAll());
		editor.putInt(LocalCache.OSTALA_USED, podatki.getOstalaOmrUsed());
		
		editor.putString(LocalCache.REFRESH_DATE , podatki.getRefreshDate());

		editor.commit();
	}

}
