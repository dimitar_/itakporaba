package dime.android.mobitel.itakdzabestporaba.receiver;

import dime.android.mobitel.itakdzabestporaba.AutoUpdater;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver 
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		String selectedIntervalStr = settings.getString("refreshInterval", "0");
		int selectedInterval = Integer.parseInt(selectedIntervalStr);

		Log.i("ItakDzabestPorabaAutoRefresh", "Boot completed. Registering auto refresh service with interval " + selectedIntervalStr);
		
		AutoUpdater.setNewAutoRefreshInterval(context, selectedInterval);
	}

}
