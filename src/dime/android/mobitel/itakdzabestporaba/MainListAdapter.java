/**
 * @author dime
 * 
 *         Dec 15, 2010 -- 7:20:06 PM
 */
package dime.android.mobitel.itakdzabestporaba;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainListAdapter extends BaseAdapter
{

	private static final String[]	titles	= {"Mesečna poraba: ", "Poraba prek Monete: ",
			"Klici v omr. Mobitel: ", "SMS in MMS: ", "Prenos podatkov: ", "Klici v druga omrežja: "};
	private static final int[]		icons		= {R.drawable.euro, R.drawable.euro, R.drawable.mobitel,
			R.drawable.sms, R.drawable.internet, R.drawable.ostala_omr};

	private LayoutInflater			mInflater;
	private Context					context;


	public MainListAdapter (Context context)
	{
		this.context = context;
		this.mInflater = LayoutInflater.from (context);
	}


	public int getCount ( )
	{
		return MainListAdapter.titles.length;
	}


	public Object getItem (int position)
	{
		return position;
	}


	public long getItemId (int position)
	{
		return position;
	}


	public View getView (int position, View convertView, ViewGroup parent)
	{
		// A ViewHolder keeps references to children views to avoid unnecessary calls
		// to findViewById() on each row.
		ViewHolder holder;

		// When convertView is not null, we can reuse it directly, there is no need
		// to reinflate it. We only inflate a new View when the convertView supplied
		// by ListView is null.
		if (convertView == null)
		{
			convertView = mInflater.inflate (R.layout.bar_list_item, null);

			// Creates a ViewHolder and store references to the two children views
			// we want to bind data to.
			holder = new ViewHolder ( );
			holder.text_title = (TextView) convertView.findViewById (R.id.text_title);
			holder.progress_bar = (ProgressBar) convertView.findViewById (R.id.progress_bar);
			holder.text_desc = (TextView) convertView.findViewById (R.id.text_desc);
			holder.image_logo = (ImageView) convertView.findViewById (R.id.image_logo);

			convertView.setTag (holder);
		}
		else
		{
			// Get the ViewHolder back to get fast access to the TextView
			// and the ImageView.
			holder = (ViewHolder) convertView.getTag ( );
		}
		
		if (position == 0 || position == 1)
		{
			/*
			 * Hide the progress bar
			 */
			holder.progress_bar.setVisibility (View.GONE);
		}

		holder.image_logo.setImageDrawable (this.context.getResources ( ).getDrawable (
				MainListAdapter.icons[position]));
		holder.text_title.setText (MainListAdapter.titles[position]);

		// holder.progress_bar.setMax (this.data.getMaxProgress (tv));
		// holder.progress_bar.setProgress (this.data.getProgress (tv));
		holder.text_desc.setText (MainListAdapter.titles[position]);

		return convertView;
	}

	/**
	 * The ViewHolder class
	 */
	static class ViewHolder
	{
		TextView		text_title;
		TextView		text_desc;
		ProgressBar	progress_bar;
		ImageView	image_logo;
	}

}
