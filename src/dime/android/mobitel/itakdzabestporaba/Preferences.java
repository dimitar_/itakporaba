package dime.android.mobitel.itakdzabestporaba;

import dime.android.mobitel.itakdzabestporaba.widget.ItakPorabaWidgetService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class Preferences extends PreferenceActivity implements OnSharedPreferenceChangeListener
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		// Set up a listener whenever a key changes
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		// Unregister the listener whenever a key changes
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
	{
		if (key.equals("refreshInterval"))
		{
			String selectedIntervalStr = sharedPreferences.getString(key, "0");
			int selectedInterval = Integer.parseInt(selectedIntervalStr);

			AutoUpdater.setNewAutoRefreshInterval(getApplicationContext(), selectedInterval);
		}
		else if (key.equals("widgetTransparentBackground"))
		{
			// Update the widget
			getApplicationContext().startService(new Intent(getApplicationContext(), ItakPorabaWidgetService.class));
		}
	}
}
