package dime.android.mobitel.itakdzabestporaba.widget;

import dime.android.mobitel.itakdzabestporaba.AutoUpdater;
import dime.android.mobitel.itakdzabestporaba.LocalCache;
import dime.android.mobitel.itakdzabestporaba.Main;
import dime.android.mobitel.itakdzabestporaba.PlanetParser;
import dime.android.mobitel.itakdzabestporaba.Podatki;
import dime.android.mobitel.itakdzabestporaba.R;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;

public class ItakPorabaWidgetService extends Service
{

	private RemoteViews buildUpdate (Context context)
	{
		// Get the data from the local cache
		Podatki podatki = LocalCache.getLocalData (context);

		// Build an update that holds the updated widget contents
		RemoteViews updateViews = new RemoteViews (context.getPackageName ( ), R.layout.itak_poraba_widget);
		
		/*
		 * Add touch listener
		 */
		Intent intent = new Intent(context, Main.class);
		PendingIntent pendingIntent = PendingIntent.getActivity (context, 0, intent, 0);
		updateViews.setOnClickPendingIntent (R.id.widget_layout, pendingIntent);
		
		float poraba_skupaj = podatki.getPoraba ( ) + podatki.getPorabaMoneta ( );
		updateViews.setTextViewText (R.id.widget_poraba, Podatki.TWO_DECIMALS_FORMAT.format(poraba_skupaj) + " €");
		
		updateViews.setTextViewText (R.id.widget_mobitel, podatki.getMobitelUsed ( ) + "/" + podatki.getMobitelAll ( ));
		updateViews.setTextViewText (R.id.widget_sms, podatki.getSmsUsed ( ) + "/" + podatki.getSmsAll ( ));
		updateViews.setTextViewText (R.id.widget_internet, podatki.getInternetUsed ( ) + "/" + podatki.getInternetAll ( ));
		updateViews.setTextViewText (R.id.widget_ostala, podatki.getOstalaOmrUsed ( ) + "/" + podatki.getOstalaOmrAll ( ));
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		boolean transparent = settings.getBoolean("widgetTransparentBackground", false);
		
		if (transparent)
			updateViews.setInt(R.id.widget_layout, "setBackgroundResource", 0);
		else
			updateViews.setInt(R.id.widget_layout, "setBackgroundResource", R.drawable.itak_poraba_widget_background);

		return updateViews;
	}


	@Override
	public void onStart (Intent intent, int startId)
	{
		Bundle bundle = intent.getExtras();
		if (bundle != null)
		{
			boolean refreshData = bundle.getBoolean(AutoUpdater.UPDATE_DATA_INTENT_EXTRA_KEY, false);
			if (refreshData)
			{
				try
				{
					Podatki podatki = PlanetParser.getData();
					LocalCache.storeLocalData(getApplicationContext(), podatki);
				}
				catch(Exception e)
				{
					Log.e("ItakPorabaWidget", "Error while fetching the data...");
				}
			}
		}
		// Build the widget update
		RemoteViews updateViews = buildUpdate (this);

		// Push update for this widget to the home screen
		ComponentName thisWidget = new ComponentName (this, ItakPorabaWidget.class);
		AppWidgetManager manager = AppWidgetManager.getInstance (this);
		manager.updateAppWidget (thisWidget, updateViews);
		
		this.stopSelf ( );
	}


	@Override
	public IBinder onBind (Intent arg0)
	{
		// We don't need to bind to this service
		return null;
	}

}
