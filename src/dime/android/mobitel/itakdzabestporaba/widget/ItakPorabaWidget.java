package dime.android.mobitel.itakdzabestporaba.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;

public class ItakPorabaWidget extends AppWidgetProvider
{
	@Override
   public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) 
	{
       // To prevent any ANR timeouts, we perform the update in a service
       context.startService(new Intent(context, ItakPorabaWidgetService.class));
   }
}
