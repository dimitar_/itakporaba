/**
 * @author dime
 * 
 *         Dec 13, 2010 -- 11:27:36 PM
 */
package dime.android.mobitel.itakdzabestporaba;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class Podatki
{
	public static final DecimalFormat TWO_DECIMALS_FORMAT = new DecimalFormat("#.##");
	public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

	private String refreshDate;
	private float poraba;
	private float porabaMoneta;
	private int mobitelUsed;
	private int mobitelAll;
	private int smsUsed;
	private int smsAll;
	private int internetUsed;
	private int internetAll;
	private int ostalaOmrUsed;
	private int ostalaOmrAll;

	public void setPoraba(float poraba)
	{
		this.poraba = poraba;
	}

	public float getPoraba()
	{
		return poraba;
	}

	public void setPorabaMoneta(float porabaMoneta)
	{
		this.porabaMoneta = porabaMoneta;
	}

	public float getPorabaMoneta()
	{
		return porabaMoneta;
	}

	public void setMobitelUsed(int mobitelUsed)
	{
		this.mobitelUsed = mobitelUsed;
	}

	public int getMobitelUsed()
	{
		return mobitelUsed;
	}

	public void setMobitelAll(int mobitelAll)
	{
		this.mobitelAll = mobitelAll;
	}

	public int getMobitelAll()
	{
		return mobitelAll;
	}

	public void setSmsUsed(int smsUsed)
	{
		this.smsUsed = smsUsed;
	}

	public int getSmsUsed()
	{
		return smsUsed;
	}

	public void setSmsAll(int smsAll)
	{
		this.smsAll = smsAll;
	}

	public int getSmsAll()
	{
		return smsAll;
	}

	public void setInternetUsed(int internetUsed)
	{
		this.internetUsed = internetUsed;
	}

	public int getInternetUsed()
	{
		return internetUsed;
	}

	public void setInternetAll(int internetAll)
	{
		this.internetAll = internetAll;
	}

	public int getInternetAll()
	{
		return internetAll;
	}

	public void setOstalaOmrUsed(int ostalaOmrUsed)
	{
		this.ostalaOmrUsed = ostalaOmrUsed;
	}

	public int getOstalaOmrUsed()
	{
		return ostalaOmrUsed;
	}

	public void setOstalaOmrAll(int ostalaOmrAll)
	{
		this.ostalaOmrAll = ostalaOmrAll;
	}

	public int getOstalaOmrAll()
	{
		return ostalaOmrAll;
	}

	public String getRefreshDate()
	{
		return refreshDate;
	}

	public void setRefreshDate(String refreshDate)
	{
		this.refreshDate = refreshDate;
	}

}
